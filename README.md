
# RvO Portfolio

<img src="https://robinvoor.com/assets/img/rvo-primary.svg" width="100" height="100">

[https://robinvoor.com](https://robinvoor.com)


## Build status

[![Netlify Status](https://api.netlify.com/api/v1/badges/ed476df1-4eb2-40bc-b4a1-6040ff6c3051/deploy-status)](https://app.netlify.com/sites/robinvoor/deploys)


## Requirements

NodeJS >=12.0.0

## Development

1. Run `npm install -g pnpm` to install pnpm if you don't have it yet
2. Run `pnpm install` to install dependencies 
3. Run `pnpm dev` to start dev server

## Build

1. Run `pnpm build`
2. Built production files are placed in the `dist` folder
