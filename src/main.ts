import { createApp } from 'vue'
import { createRouter, createWebHistory } from 'vue-router'
import generatedRoutes from 'virtual:generated-pages'
import { setupLayouts } from 'virtual:generated-layouts'
import VueLazyload from '@jambonn/vue-lazyload'
import VueTippy from 'vue-tippy'
import VueSmoothScroll from 'vue3-smooth-scroll'
import { ObserveVisibility } from 'vue-observe-visibility'

import App from './App.vue'
import 'virtual:windi.css'
import 'virtual:windi-devtools'

import 'tippy.js/dist/tippy.css'
import './styles/main.css'

const app = createApp(App)

// Setup pages with layouts
const routes = setupLayouts(generatedRoutes)
const router = createRouter({
  history: createWebHistory(),
  routes,
  scrollBehavior(to) {
    if (to.hash) {
      return {
        el: to.hash,
        behavior: 'smooth',
      }
    }
    return { top: 0 }
  },
})
app.use(router)

// https://github.com/laineus/vue3-smooth-scroll
app.use(VueSmoothScroll)

// https://github.com/jambonn/vue-lazyload
app.use(VueLazyload, {
  loading: '/assets/img/loading/spinner-primary.svg',
})

// https://github.com/KABBOUCHI/vue-tippy || https://vue-tippy.netlify.app/
app.use(VueTippy, {
  directive: 'tippy',
  component: 'tippy',
  componentSingleton: 'tippy-singleton',
},
)

// https://github.com/Akryum/vue-observe-visibility/issues/219#issuecomment-688586495
app.directive('observe-visibility', {
  beforeMount: (el, binding, vnode) => {
    (vnode as any).context = binding.instance
    ObserveVisibility.bind(el, binding, vnode)
  },
  updated: ObserveVisibility.update,
  unmounted: ObserveVisibility.unbind,
})

// install all modules under `modules/`
Object.values(import.meta.globEager('./modules/*.ts')).map(i => i.install?.({ app, router, routes }))

app.mount('#app')
