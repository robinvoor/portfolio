export default class ImageService {
  /**
   * Transform image.
   * https://www.storyblok.com/tp/storyblok-image-service-vuejs
   *
   * @param {Slug} image
   * @param {Version} option
   *
   */
  static transformImage(image, option) {
    if (!image) return ''
    if (!option) return ''

    const imageService = 'https://img2.storyblok.com/'
    const path = image.replace('https://a.storyblok.com', '')

    return imageService + option + path
  }
}
