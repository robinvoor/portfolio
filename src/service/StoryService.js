import StoryblokClient from 'storyblok-js-client'

let isInEditor = false
let isInDevMode = false

if (import.meta.env.MODE === 'development')
  isInDevMode = true

// Storyblok Bridge https://www.storyblok.com/docs/Guides/storyblok-latest-js
function loadBridge(callback) {
  const existingScript = document.getElementById('storyblokBridge')
  if (!existingScript) {
    const script = document.createElement('script')
    script.src = '//app.storyblok.com/f/storyblok-v2-latest.js'
    script.id = 'storyblokBridge'
    document.body.appendChild(script)
    script.onload = () => {
      callback()
    }
  }
  else {
    callback()
  }
}

if (window.location.search.includes('_storyblok')) {
  // load the bridge only inside of Storyblok
  loadBridge(() => {
    const { StoryblokBridge } = window
    const storyblokInstance = new StoryblokBridge()

    // 'change' to preview on save 'published' to preview after publish
    storyblokInstance.on(['change'], () => {
      location.reload(true)
    })

    isInEditor = true
  })
}

const token = 'qlFSvu9iaxoyQSPOQNPvDQtt'
const storyapi = new StoryblokClient({
  accessToken: token,
  cache: {
    clear: 'auto',
    type: 'memory',
  },
})

export default class StoryService {
  /**
   * Get all stories.
   *
   * @param {Slug} slug
   *
   * @return {Promise}
   */

  async getStory(slug) {
    let result = {}

    await storyapi.get(`cdn/stories?${slug}`, {
      version: isInEditor || isInDevMode ? 'draft' : 'published',
    }).then((response) => {
      result = response.data.stories
    }).catch((error) => {
      console.error(error)
    })

    return result
  }
}
