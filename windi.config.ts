import { defineConfig } from 'vite-plugin-windicss'

export default defineConfig({
  darkMode: 'class',
  plugins: [
    require('windicss/plugin/typography'),
  ],
  attributify: true,
  safelist: 'p-1 p-1.5 p-3',
  theme: {
    container: {
      center: true,
      padding: '1rem',
      screens: {
        'sm': '640px',
        'md': '768px',
        'lg': '1024px',
        'xl': '1024px',
        '2xl': '1024px',
      },
    },
    extend: {
      fontFamily: {
        main: ['Lato', 'sans-serif'],
      },
      fontSize: {
        '2xs': '.6rem',
      },
      typography: {
        DEFAULT: {
          css: {
            color: 'inherit',
            b: { color: 'inherit' },
            strong: { color: 'inherit' },
            em: { color: 'inherit' },
            h1: { color: 'inherit' },
            h2: { color: 'inherit' },
            h3: { color: 'inherit' },
            h4: { color: 'inherit' },
            code: { color: 'inherit' },
          },
        },
      },
    },
  },
})
